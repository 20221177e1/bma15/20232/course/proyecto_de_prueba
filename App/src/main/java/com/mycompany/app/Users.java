package com.mycompany.app;

//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>

import java.io.*;
import java.util.Scanner;

 
public class Users {

    public String[] usuarios;
    public String[] claves;
    public String[] dni;
    public double[] saldo;

    public Users(String filePath) {
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                throw new FileNotFoundException("El archivo no existe: " + filePath);
            }

            Scanner lineCounter = new Scanner(file);
            int numberOfLines = 0;
            while (lineCounter.hasNextLine()) {
                lineCounter.nextLine();
                numberOfLines++;
            }
            lineCounter.close();

            usuarios = new String[numberOfLines];
            dni = new String[numberOfLines];
            claves = new String[numberOfLines];
            saldo = new double[numberOfLines];

            Scanner scanner = new Scanner(file);
            int i = 0;
            while (scanner.hasNextLine()) {
                String[] parts = scanner.nextLine().split(",");
                if (parts.length == 4) {
                    usuarios[i] = parts[0].trim();
                    dni[i] = parts[1].trim();
                    claves[i] = parts[2].trim();
                    saldo[i] = Double.parseDouble(parts[3].trim()); // Convierte la cadena a un double
                    i++;
                } else {
                    System.out.println("Formato incorrecto en una línea");
                }
            }

            scanner.close();

        } catch (FileNotFoundException e) {
            System.out.println("Error al leer el archivo: " + e.getMessage());
        }
    }

    public String getUsuarios(int posicion) {
        return usuarios[posicion];
    }

    public String getDNI(int posicion) {
        return dni[posicion];
    }

    public String getClave(int posicion) {
        return claves[posicion];
    }

    public double getSaldo(int posicion) {
        return saldo[posicion];
    }
}

