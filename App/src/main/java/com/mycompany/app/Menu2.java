package com.mycompany.app;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>

public class Menu2 extends JFrame{
    
    public JPanel panel;
    
    public Menu2(){
        startPanel();
        startComponents();
        startEtiquetas();
        botonSalir();
        startBotones();
    }
    private void startPanel()
    {
       setSize(900, 550);
       setLocationRelativeTo(null);
       setTitle("Menu2");
       
    }
    private void startComponents(){
        panel= new JPanel();
        panel.setLayout(null);

        panel.setBackground(new Color(0, 42, 141));
        
        this.getContentPane().add(panel);
    }
    private void startEtiquetas()
    {
        //TEXTO ETIQUETA 1
        JLabel etiqueta = new JLabel();
        JLabel etiqueta1 = new JLabel();
        
        etiqueta.setText("Bienvenido");
        etiqueta1.setText("¿Qué operación quieres hacer hoy?");
                
        etiqueta.setBounds(20,-22,100,100);
        etiqueta1.setBounds(20,-200,500,500);
        
        etiqueta.setForeground(Color.white);
        etiqueta1.setForeground(Color.white);
        
        etiqueta.setFont(new Font("Helvetica",Font.BOLD,12));
        etiqueta1.setFont(new Font("Helvetica",Font.BOLD,18));
       
        panel.add(etiqueta);
        panel.add(etiqueta1);
    
    }
    private void botonSalir(){
        JButton boton1 = new JButton();
        boton1.setBounds(800,20,40,40);
        boton1.setEnabled(true);//encendido true , apagado false
        boton1.setForeground(Color.white);
        ImageIcon clicAqui=new ImageIcon("C:/Users/USUARIO/Documents/NetBeansProjects/BancoBCP/App/images/exit.png");
        boton1.setIcon(new ImageIcon(clicAqui.getImage().getScaledInstance(boton1.getWidth(),boton1.getHeight(),Image.SCALE_SMOOTH)));
        boton1.setFont(new Font("arial",3,12));
        
        // Agregar ActionListener al botón
        boton1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            // Cerrar la ventana y finalizar el programa
            dispose(); // Cierra la ventana actual
            System.exit(0); // Finaliza el programa
        }
        });
        
        panel.add(boton1);
    }
    public void startBotones(){
        
        JButton boton1 = new JButton();
        JButton boton2 = new JButton();
        
        boton1.setBounds(20,100,240,240);
        boton2.setBounds(270,100,240,120);
        
        boton1.setEnabled(true);//encendido true , apagado false
        boton2.setEnabled(true);//encendido true , apagado false
        
        ImageIcon clicAqui=new ImageIcon("C:/Users/USUARIO/Documents/NetBeansProjects/BancoBCP/App/images/retirarefectivo.jpg");
        ImageIcon clicAqui2=new ImageIcon("C:/Users/USUARIO/Documents/NetBeansProjects/BancoBCP/App/images/transferirefectivo.png");
       
        boton1.setIcon(new ImageIcon(clicAqui.getImage().getScaledInstance(boton1.getWidth(),boton1.getHeight(),Image.SCALE_SMOOTH)));
        boton2.setIcon(new ImageIcon(clicAqui2.getImage().getScaledInstance(boton2.getWidth(),boton2.getHeight(),Image.SCALE_SMOOTH)));
        
        // Agregar ActionListener a boton1
        boton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose(); // Cierra la ventana actual

                // Abre la nueva ventana de otra clase
                VentanaSolesDolares vsd = new VentanaSolesDolares();
                vsd.setVisible(true);
            }
        });

        // Agregar ActionListener a boton2
        boton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose(); // Cierra la ventana actual

                // Abre la nueva ventana de otra clase
                VentanaSolesDolares vsd = new VentanaSolesDolares();
                vsd.setVisible(true);
            }
        });
        
        
        panel.add(boton1);
        panel.add(boton2);
        
    }
    
    
    
}
