package com.mycompany.app;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>

public class SegundaVentana extends JFrame {
    
    public JPanel panel;
    public String dni;
    
    public SegundaVentana(){
       startPanel();
       startComponents();
       startEtiquetas();
       obtenerDNI();
       botonSalir();
    }
    private void startPanel(){
       setSize(900, 550);
       setLocationRelativeTo(null);
       setTitle("Ingresa DNI");
    }
    private void startComponents(){
        panel= new JPanel();
        panel.setLayout(null);

        panel.setBackground(new Color(3, 35, 110,255));
        
        this.getContentPane().add(panel);
       
    }
    private void startEtiquetas()
    {
        JLabel etiqueta = new JLabel();
        JLabel etiqueta1 = new JLabel();
   
        etiqueta.setText("Ingresa tu numero de DNI");
        etiqueta1.setText("para empezar tu sesión");
        
        etiqueta.setBounds(60,-40,300,300);
        etiqueta1.setBounds(60,-10,300,300);
        
        etiqueta.setForeground(Color.white);
        etiqueta1.setForeground(Color.white);
        
        etiqueta.setFont(new Font("Helvetica",Font.BOLD,22));
        etiqueta1.setFont(new Font("Helvetica",Font.BOLD,15));
        
        panel.add(etiqueta);
        panel.add(etiqueta1);
        
        JLabel etiqueta2 = new JLabel(new ImageIcon("C:/Users/USUARIO/Documents/NetBeansProjects/BancoBCP/App/images/dni.jpg"));
        etiqueta2.setBounds(494,0,386,550);
        panel.add(etiqueta2);
    }
    
    private void obtenerDNI() {
        JTextField cajaTexto = new JTextField();
        cajaTexto.setBounds(60, 180, 150, 40);
        panel.add(cajaTexto);

        cajaTexto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dni = cajaTexto.getText();
                if (validarDNI()) {
                    // DNI válido, cerrar esta ventana y abrir la siguiente
                    dispose(); // Cierra la ventana actual
                    // Aquí debes abrir la nueva ventana de la otra clase
                    Menu2 menu2 = new Menu2();
                    menu2.setVisible(true);
                    // ...
                } else {
                    // DNI no válido, mostrar mensaje de error
                    JOptionPane.showMessageDialog(null, "DNI no válido", "Error", JOptionPane.ERROR_MESSAGE);
                    // Puedes limpiar el JTextField si lo deseas
                    cajaTexto.setText("");
                }
            }
        });
    }
    private boolean validarDNI() {
        // Lógica para comparar el DNI ingresado con los DNIs en tu arreglo
        // Supongamos que tienes una instancia de la clase Users llamada "usuariosObj"
        Users usuariosObj = new Users("C:/Users/USUARIO/Documents/NetBeansProjects/BancoBCP/App/clientes/clientes.txt");
        String[] dnisArray = usuariosObj.dni;

        for (String dniEnArray : dnisArray) {
            if (dni.equals(dniEnArray)) {
                return true; // DNI válido
            }
        }

        return false; // DNI no válido
    }
    private void botonSalir(){
        JButton boton1 = new JButton();
        boton1.setBounds(440,20,40,40);
        boton1.setEnabled(true);//encendido true , apagado false
        ImageIcon clicAqui=new ImageIcon("C:/Users/USUARIO/Documents/NetBeansProjects/BancoBCP/App/images/exit.png");
        boton1.setIcon(new ImageIcon(clicAqui.getImage().getScaledInstance(boton1.getWidth(),boton1.getHeight(),Image.SCALE_SMOOTH)));
        
        // Agregar ActionListener al botón
        boton1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            // Cerrar la ventana y finalizar el programa
            dispose(); // Cierra la ventana actual
            System.exit(0); // Finaliza el programa
        }
        });
        
        panel.add(boton1);
    }
    
}
