package com.mycompany.app;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>
 
public class VentanaSolesDolares extends JFrame{
    
    public JPanel panel;
            
    public VentanaSolesDolares(){
        startPanel();
        startComponents();
        startEtiquetas();
        botonCancelar();
    }
    private void startPanel()
    {
       setSize(900, 550);
       setLocationRelativeTo(null);
       setTitle("Menu2");
       
    }
    private void startComponents(){
        panel= new JPanel();
        panel.setLayout(null);

        panel.setBackground(Color.white);
        
        this.getContentPane().add(panel);
    }
    private void startEtiquetas()
    {
        //TEXTO ETIQUETA 1
        JLabel etiqueta = new JLabel();
        JLabel etiqueta1 = new JLabel();
        
        etiqueta.setText("Retiro de efectivo");
        etiqueta1.setText("Selecciona una de tus cuentas");
                
        etiqueta.setBounds(40,-22,100,100);
        etiqueta1.setBounds(40,-200,500,500);
        
        etiqueta.setForeground(Color.black);
        etiqueta1.setForeground(Color.black);
        
        etiqueta.setFont(new Font("Helvetica",Font.BOLD,12));
        etiqueta1.setFont(new Font("Helvetica",Font.BOLD,18));
       
        panel.add(etiqueta);
        panel.add(etiqueta1);
    
    }
    private void botonCancelar(){
        JButton boton1 = new JButton("Cancelar");
        boton1.setBounds(750,20,100,40);
        boton1.setEnabled(true);//encendido true , apagado false
        boton1.setForeground(Color.black);
        boton1.setFont(new Font("arial",3,12));
        
        // Agregar ActionListener al botón
        boton1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            // Cerrar la ventana y finalizar el programa
            dispose(); // Cierra la ventana actual
            Menu2 menu2 = new Menu2();
            menu2.setVisible(true);//abre la ventana anterior
        }
        });
        
        panel.add(boton1);
    }
}
