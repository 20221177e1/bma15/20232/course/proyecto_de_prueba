package com.mycompany.app;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>

public class Menu1 extends JFrame{
    
    public JPanel panel;
    
    public Menu1(){
        setSize(900, 550);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Menu");
        startComponents();
        colocarBotones();
    }
    private void startComponents(){
        panel= new JPanel();
        panel.setLayout(null);

        panel.setBackground(new Color(0, 42, 141));
        
        this.getContentPane().add(panel);
        
        JLabel etiqueta2 = new JLabel(new ImageIcon("C:/Users/USUARIO/Documents/NetBeansProjects/BancoBCP/App/images/anuncio.jpg"));
        etiqueta2.setBounds(0,25,890,460);
        panel.add(etiqueta2);
    }
    private void colocarBotones(){
        JButton boton1 = new JButton();
        boton1.setBounds(638,440,100,40);
        boton1.setEnabled(true);//encendido true , apagado false
        ImageIcon clicAqui=new ImageIcon("C:/Users/USUARIO/Documents/NetBeansProjects/BancoBCP/App/images/boton.png");
        boton1.setIcon(new ImageIcon(clicAqui.getImage().getScaledInstance(boton1.getWidth(),boton1.getHeight(),Image.SCALE_SMOOTH)));
        
        boton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Este código se ejecutará cuando se haga clic en el botón
                // Cerrar la ventana al hacer clic en el botón
                cerrarVentanaYAbrirNueva();
            }
        });
        
        
        panel.add(boton1);
    }
    private void cerrarVentanaYAbrirNueva() {
        // Cerrar la ventana actual
        dispose();
        
        // Crear una nueva instancia de la segunda ventana (otra clase)
        SegundaVentana nuevaVentana = new SegundaVentana();
        nuevaVentana.setVisible(true);
    }
    
}
