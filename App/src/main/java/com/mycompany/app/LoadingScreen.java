package com.mycompany.app;
import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

//@author Emerson Espinoza <emerson.espinoza.s@uni.pe>

public class LoadingScreen extends JFrame{
    
    public JPanel panel;
    
    public LoadingScreen(){
        startPanel();
        startComponents();
        startEtiquetas();
    }
    private void startPanel()
    {
       setSize(900, 550);
       setLocationRelativeTo(null);
       setTitle("Loading Screen");
    }
    
    private void startComponents(){
        panel = new JPanel();
        panel.setLayout(null);

        panel.setBackground(new Color(2, 28, 87,255));
        
        this.getContentPane().add(panel);
    }
    private void startEtiquetas()
    {
        //TEXTO ETIQUETA 1
        JLabel etiqueta = new JLabel();
        etiqueta.setText("<html>Espere un momento<br>por favor<br>____</html>");
        etiqueta.setBounds(50,120,250,250);
        etiqueta.setForeground(Color.white);
        etiqueta.setFont(new Font("Helvetica",Font.BOLD,22));
        panel.add(etiqueta);
        
        //IMAGENES ETIQUETA 2
        JLabel etiqueta2 = new JLabel(new ImageIcon("C:/Users/USUARIO/Documents/NetBeansProjects/BancoBCP/App/images/logobcp.png"));
        JLabel carga = new JLabel(new ImageIcon("C:/Users/USUARIO/Documents/NetBeansProjects/BancoBCP/App/images/carga.jpg"));
        etiqueta2.setBounds(700,10,120,80);
        carga.setBounds(0,0,890,509);
        panel.add(etiqueta2);
        panel.add(carga);
    }
}
